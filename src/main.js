import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from './router'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

//Для scss
// import Vue from 'vue'
// import { BootstrapVue } from 'bootstrap-vue'
// import './app.scss'
// Vue.use(BootstrapVue)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

// Это главный файл который запускает все приложение
// В new Vue мы создаем новый экземпляр Vue
// и с помощью метода render рендерим App компонент, который импортируем из файла /App.vue (2 строчка)
// после mount'им в #app (в папке public index <div id="app")